package ru.tsc.gavran.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.command.AbstractCommand;

public class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @Nullable
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list of commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        int index = 0;
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            index++;
            System.out.println(index + ". " + command.toString());
        }
    }

}