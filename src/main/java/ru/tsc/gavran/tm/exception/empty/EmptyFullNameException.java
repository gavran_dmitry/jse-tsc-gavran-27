package ru.tsc.gavran.tm.exception.empty;

import ru.tsc.gavran.tm.exception.AbstractException;

public class EmptyFullNameException extends AbstractException {

    public EmptyFullNameException() {
        super("Error! Full name cannot be empty.");
    }

}
