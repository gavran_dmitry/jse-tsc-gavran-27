package ru.tsc.gavran.tm.exception.entity;

import ru.tsc.gavran.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found.");
    }

}