package ru.tsc.gavran.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.model.Project;
import ru.tsc.gavran.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    @NotNull
    List<Task> findTaskByProjectId(@NotNull String userId, String projectId);

    @NotNull
    Task bindTaskById(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    @NotNull
    Task unbindTaskById(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    @Nullable
    Project removeProjectById(@NotNull String userId, @Nullable String projectId);

    @Nullable
    Project removeProjectByIndex(@NotNull String userId, @NotNull int index);

    @Nullable
    Project removeProjectByName(@NotNull String userId, @Nullable String name);

}
