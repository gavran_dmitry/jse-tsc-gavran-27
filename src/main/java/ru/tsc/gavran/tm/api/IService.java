package ru.tsc.gavran.tm.api;

import ru.tsc.gavran.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}